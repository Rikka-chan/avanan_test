FROM python:3.8
ENV PYTHONUNBUFFERED=1
WORKDIR /code

COPY requirements.pip /code/
RUN pip install -r requirements.pip
COPY src/avanan_dlp /code/
COPY entrypoint.sh /code/entrypoint.sh

RUN chmod a+x /code/entrypoint.sh
RUN cp /code/entrypoint.sh /usr/local/bin/entrypoint.sh

ENTRYPOINT entrypoint.sh