import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'avanan_dlp.settings')

app = Celery('avanan_dlp')

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
