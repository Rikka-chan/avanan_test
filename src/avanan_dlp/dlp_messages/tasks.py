from django.conf import settings

from celery import shared_task

from .models import SensitiveMessage, MessageFilter
from .utils import load_reflector
from .readers.message import Message
from .reflectors.base import BaseReflector


@shared_task
def reflect_on_message(reflector_conf, message_data):
    message = Message.load(message_data)
    reflector: BaseReflector = load_reflector(reflector_conf)
    reflector.reflect(message)


def process_reflections(message_data):
    msg_type = message_data['type']

    reflectors = settings.MESSAGE_REFLECTORS
    if reflectors:
        reflectors = reflectors.get(msg_type, [])
        for reflector_conf in reflectors:
            reflect_on_message.delay(
                reflector_conf=reflector_conf,
                message_data=message_data
            )


@shared_task
def check_message(message_data):
    content = message_data['content']
    date_time = message_data['date_time']

    filters = MessageFilter.objects.all()
    for msg_filter in filters.iterator():
        if msg_filter.expression.match(content):
            message = SensitiveMessage(
                content=content,
                date_time=date_time,
                filter=msg_filter,
            )
            message.save()

            process_reflections(message_data)
