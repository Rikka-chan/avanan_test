from abc import ABC, abstractmethod
from threading import Event, Thread
from typing import Callable


from .message import Message


class BaseReader(Thread, ABC):
    def __init__(
            self,
            callback: Callable,
            msg_type: str = None,
            use_dicts: bool = False,
     ):
        super(BaseReader, self).__init__()

        self.callback = callback
        self.msg_type = msg_type
        self.use_dicts = use_dicts

        self._stop_event = Event()

    def is_stopped(self):
        return self._stop_event.is_set()

    def stop(self):
        self._stop_event.set()

    def run(self):
        self._reading()

    @abstractmethod
    def _reading(self):
        pass

    def process_message(self, message: Message):
        if message.type is None:
            message.type = self.msg_type

        if self.use_dicts:
            message = message.dict()

        self.callback(message)
