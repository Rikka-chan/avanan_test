import logging

from datetime import datetime

from slack_sdk.web import WebClient
from slack_sdk.socket_mode import SocketModeClient
from slack_sdk.socket_mode.response import SocketModeResponse
from slack_sdk.socket_mode.request import SocketModeRequest

from .base import BaseReader
from .message import Message


class SlackReader(BaseReader):
    def __init__(
            self,
            app_token: str,
            bot_token: str,
            *args,
            **kwargs,
    ):
        super().__init__(*args, **kwargs)

        self.client = SocketModeClient(
             app_token=app_token,
             web_client=WebClient(token=bot_token),
        )

    def process_request(
            self,
            client: SocketModeClient,
            req: SocketModeRequest,
    ):
        try:
            if req.type == "events_api":
                response = SocketModeResponse(envelope_id=req.envelope_id)
                client.send_socket_mode_response(response)
                if req.payload["event"]["type"] == "message" \
                   and req.payload["event"].get("subtype") is None:
                    message = Message(
                        content=req.payload["event"]["text"],
                        date_time=datetime.fromtimestamp(float(req.payload["event"]["ts"])),
                        meta=req.payload,
                    )

                    self.process_message(message)
        except Exception as ex:
            logging.exception(ex)

    def _reading(self):
        self.client.socket_mode_request_listeners.append(self.process_request)
        self.client.connect()
