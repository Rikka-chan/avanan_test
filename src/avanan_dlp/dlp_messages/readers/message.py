from dataclasses import dataclass, asdict, field
from datetime import datetime


@dataclass
class Message:
    content: str
    date_time: datetime
    type: str = None
    meta: dict = field(default_factory=dict)

    def dict(self):
        return asdict(self)

    @classmethod
    def load(cls, data):
        return cls(**data)
