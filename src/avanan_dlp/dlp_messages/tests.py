from datetime import datetime

from django.test import TestCase

from .models import MessageFilter, SensitiveMessage

from .tasks import check_message


TEST_MESSAGES = [
    'qwe-123',
    '-qwe123',
    '-qweq123',
    '123-qwe123-',
]


class MessagesTest(TestCase):
    def setUp(self) -> None:
        self.settings(
            DATABASES={
                'default': {
                    'ENGINE': 'django.db.backends.sqlite3',
                    'NAME': ':memory:',
                }
            }
        )
        filter = MessageFilter(
            expression='.*-qwe\\d',
            description='test filter'
        )
        filter.save()

    def test_checking_messages(self):
        test_messages = [
            'qwe-123',
            '-qwe123',
            '-qweq123',
            '123-qwe123-',
        ]

        for msg in test_messages:
            msg = dict(
                content=msg,
                date_time=datetime.now(),
                type=''
            )
            check_message(msg)

        self.assertEqual(SensitiveMessage.objects.all().count(), 2)
