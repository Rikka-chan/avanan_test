import signal
import sys

from django.apps import AppConfig
from django.conf import settings

from . import utils


class DlpMessagesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dlp_messages'
    readers = []

    def ready(self):
        from .tasks import check_message

        readers_conf = settings.MESSAGE_READERS
        if not readers_conf:
            return

        for reader_conf in readers_conf:
            reader = utils.load_reader(
                reader_conf,
                callback=check_message.delay,
            )
            reader.start()
            self.readers.append(reader)

        def stop_readers(sig, frame):
            for reader in self.readers:
                reader.stop()
                reader.join()
            sys.exit(0)

        signal.signal(signal.SIGTERM, stop_readers)
        signal.signal(signal.SIGINT, stop_readers)
