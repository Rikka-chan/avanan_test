from django.db import models
from regex_field.fields import RegexField


class MessageFilter(models.Model):
    expression = RegexField(max_length=1024)
    description = models.CharField(max_length=512)

    class Meta:
        app_label = 'dlp_messages'


class SensitiveMessage(models.Model):
    content = models.TextField()
    date_time = models.DateTimeField()
    filter = models.ForeignKey(
        MessageFilter,
        on_delete=models.RESTRICT,
    )
