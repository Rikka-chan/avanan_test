from slack_sdk.web import WebClient

from .base import BaseReflector
from ..readers.message import Message


class SlackReflector(BaseReflector):
    def __init__(
            self,
            token: str,
    ):
        self.client = WebClient(token=token)

    def reflect(self, message: Message):
        channel = message.meta['event']['channel']
        timestamp = message.meta['event']['ts']

        self.client.chat_update(
            channel=channel,
            ts=timestamp,
            text='The original message has been blocked.',
        )
