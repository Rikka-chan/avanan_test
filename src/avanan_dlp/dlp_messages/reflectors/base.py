from abc import ABC, abstractmethod

from ..readers.message import Message


class BaseReflector(ABC):
    @abstractmethod
    def reflect(self, message: Message):
        pass
