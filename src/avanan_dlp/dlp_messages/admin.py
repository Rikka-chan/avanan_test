from django.contrib import admin
from .models import SensitiveMessage, MessageFilter


class SensitiveMessageAdmin(admin.ModelAdmin):
    model = SensitiveMessage
    list_display = ["content", "date_time", "get_expression"]

    def get_expression(self, obj):
        return obj.filter.expression

    get_expression.admin_order_field = "filter_expression"
    get_expression.short_description = "Filter's expression"


admin.site.register(SensitiveMessage, SensitiveMessageAdmin)
admin.site.register(MessageFilter)
