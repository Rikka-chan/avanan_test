import importlib

from .readers.base import BaseReader
from .reflectors.base import BaseReflector


def load_class(import_path):
    path_parts = import_path.split('.')
    module_path = '.'.join(path_parts[:-1])
    class_name = path_parts[-1]

    module = importlib.import_module(module_path)
    cls = getattr(module, class_name)
    return cls


def load_instance(
        instance_conf: dict,
        expected_class: type = None,
        instance_name: str = '',
        **kwargs,
):
    reader_class = load_class(instance_conf['cls'])
    reader_options = instance_conf.get('options', {})
    if expected_class is not None and not issubclass(reader_class, expected_class):
        raise TypeError(
            f'Message {instance_name} class should be subclass '
            f'of {expected_class.__name__}, got {reader_class.__name__}')

    return reader_class(**kwargs, **reader_options)


def load_reader(
        reader_conf: dict,
        **kwargs,
):
    return load_instance(
        instance_conf=reader_conf,
        expected_class=BaseReader,
        instance_name='reader',
        **kwargs,
    )


def load_reflector(
        reflector_conf: dict,
        **kwargs,
):
    return load_instance(
        instance_conf=reflector_conf,
        expected_class=BaseReflector,
        instance_name='reflector',
        **kwargs,
    )
