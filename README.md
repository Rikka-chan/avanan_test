#This is the test project for avanan

###Setting up

Firstly, you need to specify SLACK_APP_TOKEN and SLACK_USER_TOKEN in .env.dev file.
Example:

```.env
SLACK_APP_TOKEN=xapp-...
SLACK_USER_TOKEN=xoxp-...
```

Further customizations can be done by editing this file.

### Launching
You can start project by start docker-compose services.

```shell script
docker-compose up
```


### Testing

```shell script
cd src/avanan_dlp
./manage.py test
```


### Development
Install development requirements
```shell script
pip install -r requirements.dev.pip
```

Use pre-commit module to install git hooks
```shell script
pre-commit install
```

### Custom Reader/Reflector
You can use BaseReader and BaseReflector class to implement your custom behaviour.
Don't forget to register your classes in the MESSAGE_READERS and MESSAGE_REFLECTORS sections of the project settings.
Example:
```python
MESSAGE_READERS = [
    {
        'cls': 'your_module.YourReader',
        'options': {
            'custom_option': 'Some value for option',
            'msg_type': 'custom',
        }
    },
]

MESSAGE_REFLECTORS = {
    'slack': [
        {
            'cls': 'your_module.YourkReflector',
            'options': {
                'custom_option': 'Some value for option',
            }
        }
    ]
}
```